from functools import reduce
from lxml import html
import requests
import re
import sys
import os
import codecs


class Product:
    def __init__(self, site, category, price, picture_url, product_url):
        self.site = site
        self.category = category
        self.price = price
        self.picture_url = picture_url
        self.product_url = product_url

    def as_string(self):
        return self.site + "/" + self.category + ": " + self.price + " - " + self.product_url

    def as_json(self):
        return '{"site":"' + self.site + '", "category":"' + self.category + '", "price":' + str(
                to_number(self.price)) + \
               ', "picture_url": "' + self.picture_url + '", "product_url": "' + self.product_url + '"}'

    def get_url(self):
        return self.product_url


def to_number(string):
    numbers = re.findall('\d+', string)
    if len(string.strip()) > 0:
        return int(''.join(numbers))


def contains(array, find):
    for x in array:
        if find(x):
            return True
    return False


def get_colored_status(status_code):
    if status_code == '200':
        return "\033[92mHTTP [" + status_code + "]\033[0m"
    else:
        return "\033[91mHTTP [" + status_code + "]\033[0m"


os.system('cls')
total_percent = 0
base = "http://www2.hm.com"
products = []
main_sites = ["noi", "ferfi", "gyerekek", "home", "learazas"]
product = '//article[@class="product-item clickable-container has-link"]'
description_selector = product + '/h3[@class="product-item-headline"]/a/text()'
product_url = product + '/h3[@class="product-item-headline"]/a/@href'
price_selector = product + '/div[@class="product-item-price"]/text()'
image_selector = product + '/div[@class="product-item-image-wrapper"]/img[1]/@src'


def print_status(main, sub, status, completed):
    sys.stdout.write("\r\x1b[K" +
                     get_colored_status(str(status)) +
                     " \033[95mScraping\033[0m @ " + str(completed) + "% " + "\033[92m" +
                     main + "\033[0m/\033[93m" +
                     sub +
                     "\033[0m... ")
    sys.stdout.flush()


def get_completed_percent(cur, items, every):
    return int(((cur / items) * 100) / every)

for site in main_sites:
    page = requests.get("http://www2.hm.com/hu_hu/" + site + ".html")
    tree = html.fromstring(page.content)
    categories = tree.xpath('//li[@class="section-menu-subdepartment "]/a/@href')

    index = 1
    for category in categories:
        current = requests.get(base + category + '?offset=0&page-size=3000')
        subsite = html.fromstring(current.content)

        if site == "learazas":
            price_selector = product + '/div[@class="product-item-price product-item-price-discount"]/text()'

        category_name = category.split("/")
        category_friendly = category_name[len(category_name) - 1].split(".")[0]
        completed = total_percent + get_completed_percent(index, len(categories), len(main_sites))
        print_status(site, category_friendly, current.status_code, completed)

        descriptions = subsite.xpath(description_selector)
        product_urls = subsite.xpath(product_url)
        prices = subsite.xpath(price_selector)
        images = subsite.xpath(image_selector)

        if site == "learazas":
            prices = prices[::2]

        prices_as_number = [to_number(price.strip()) for price in prices]
        if len(prices_as_number):
            average_category_price = reduce(lambda x, y: x + y, prices_as_number) / len(prices_as_number)

        # mistaken_price = int(average_category_price) * 0.05
        mistaken_price = 400

        for d, pu, pr, im in zip(descriptions, product_urls, prices, images):
            product_item = Product(site, d, pr.strip(), im, base + pu)
            if to_number(pr.strip()) < mistaken_price \
                    and not contains(products, lambda exists: exists.product_url == product_item.get_url()):
                products.append(product_item)

        index += 1
    total_percent += int(100 / len(main_sites))

with codecs.open("results.json", "w", "utf-8-sig") as file:
    file.seek(0)
    file.truncate()
    file.write("var results = [")
    for product in products:
        file.write(product.as_json() + ",")
    file.write("]")
