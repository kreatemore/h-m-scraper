# Web Scraper #

## Installation ##

**Requirements:**

    * Python 3.5.2 or newer
    * lxml and requests packages (pip install them)
    * npm install to resolve dependencies (this is optional if you want the HTML preview - current dependency is vue.js)

## Run ##

If you have the Python executable defined on your path, just type `run.py`
in your command line.

This will automatically fetch the items from the H&M Hungarian Webshop, and give a list of results back for the items that have a low price set.

You can configure the treshold by changing the value of `mistaken_price`.

There's a formula, that takes the average price of a category, and looks for values that are 5% or less of the average.
To enable this, set `mistaken_price` to `mistaken_price = int(average_category_price) * 0.05`